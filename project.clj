(defproject megatron "1.0.1"
  :description "XSLT Transformer for Connect"
  :url "https://github.appliedsystems.com/IVANS/megatron"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.zip "0.1.3"]
                 [org.clojure/data.xml "0.0.8"]
                 [clojure-saxon "1.0.0"]
                 [clojure-xalan "0.9.4"]]
  :resource-paths ["resources/"]
  :repositories {"local" "file:maven_repository"}
  :aot [com.ivans.xslt.megatron.transformer com.ivans.xslt.megatron.saxon com.ivans.xslt.megatron.xalan com.ivans.xslt.megatron]
  ; :main ^:skip-aot com.ivans.xslt.megatron.core
  :release-tasks [["vcs" "assert-committed"]
                  ["vcs" "tag" "--no-sign"]
                  ["vcs" "push"]]
  :target-path "target/%s"
  :codox {:output-path "docs"}
  :repl-options {:init-ns com.ivans.xslt.megatron.saxon}
  :test-refresh {:notify-command ["terminal-notifier" "-title" "megatron test results" "-appIcon" "img/decept.png" "-message"]}
  :profiles {:uberjar {:aot [com.ivans.xslt.megatron.transformer com.ivans.xslt.megatron.saxon com.ivans.xslt.megatron.xalan com.ivans.xslt.megatron]
                       :uberjar-exclusions [#"com/nxtech/.*"]}
             :test {:resource-paths ["test/resources/"]}
             :dev {:resource-paths ["resources/"]}})
