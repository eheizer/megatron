# Megatron

<img src="img/1460155.jpg" alt="More than meets the eye" align="right" height="601" width="281"/>
Megatron is a Clojure library aimed at handling XSLT transformations and switching between the Saxon API (for XSLT 2.0+) and Xalan (for XSLT 1.0).

## Java API

### com.ivans.xslt.megatron.Transformer

This runs transformations depending on stylesheet version and input type.  For XML input, it will transform using Saxon or Xalan depending on the stylesheet version (1.0 for Xalan, Saxon for anything else).

For non-XML input, which only Saxon supports, it will set the file path as a template parameter named `input` and then call `xsl:initial-template` on the stylesheet.  The expectation with non-XML input is that the stylesheet will read it in by URI using `unparsed-text()`, `unparsed-text-lines()`, or `json-doc()`, and then process it from there.

#### Configuration

The constructor takes a map with the following options:

|Option                     |Type                               |Description                                                                                      |
|---------------------------|-----------------------------------|-------------------------------------------------------------------------------------------------|
|`xml-input?`                 | Boolean                           | Defaults to `true`. Set to `false` if the input is JSON, CSV, or any other format that isn't valid XML 
|`error-listener`             | ErrorListener                     | An error listener receives any exceptions that occurred during stylesheet execution.  In XSLT 1.0, the error listener was also used to report xsl:message instructions.                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|`message-listener`           | MessageListener, MessageListener2 | MessageListener is a Saxon interface that exclusively applies to xsl:message instructions.  If this is provided for a 1.0 transformation, Xalan messages will be forwarded here.                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|`uri-resolver`               | URIResolver                       | This interface accepts a base/relative URI and returns a Source.  It's used to resolve URIs in the stylesheet that are expected to return XML, including `xsl:include`, `xsl:import`, `doc()` and `document()`.  Use this when the stylesheet is using URIs that don't match the filesystem (e.g., if an AgencyDatabase is created on the fly, in memory, the "AgencyDatabase.xml" URI could be resolved to a StreamSource created using the AgencyDatabase XML, or a StreamSource representing only the current agent's data.                                                                                               |
|`unparsed-text-uri-resolver` | UnparsedTextURIResolver           | This interface accepts a base/relative URI and returns a Reader.  It's used for any URIs that return unparsed text, including the arguments to `json-doc()`, `unparsed-text()`, and `unparsed-text-lines()`.  Use this if the stylesheet is expecting a URI but the resource doesn't exist on the filesystem (e.g., you can open a StringReader and have the resolver return it for an arbitrary or preset URI).                                                                                                                                                                                                                               |
|`stylesheets`                | Iterable                          | A list of stylesheets as File, URI, or URL objects
|`static-params`              | Map                               | Static parameters apply only to XSLT 3.0 and they are set on the compiler.  They can be used to affect stylesheet compilation, among other things, so they might be used to send an environment ("test" or "prod") or any facts that are known at compile time, like possibly the agent information.  If static params need to change, you have to reset them on the compiler or create a new compiler.  In Megatron this translates to creating a new Transformer instance with different static-param options.  This is passed as a map where the keys are strings or QNames and the values are any Java type coercible to XdmValues. |
|`stylesheet-params`          | Map                               | Any global (stylesheet-level) parameters to be set. The keys are treated as strings, and the values can be any Java type (including org.w3c.dom.Node).  Unlike with static parameters, these cannot be used to affect compilation.  For multiple stylesheets, the parameters are set on each stylesheet.                                                                                                                                                                                                                                                                                                                                   |


#### Constructor
```java
   Transformer(java.util.Map configuration)
```
#### Methods

```java
   String transform([File, URI, URL, or Source] input)
   void transform(File input, File output)
```

### Documentation

Clojure docs are [here](https://github.appliedsystems.com/pages/IVANS/megatron/).

## To-do

* `xsl:package` support

## License

Copyright © 2020 IVANS Insurance Solutions, Inc.
