(ns com.ivans.xslt.megatron
  "The Transformer is a generated facade for transforming stylesheets
  using Saxon (for XSLT 3.0) or Xalan (for 1.0)"
  (:require [clojure.data.xml :as xml]
            [com.ivans.xslt.megatron.core :as mc]
            [saxon.core :as sc])
  (:import javax.xml.transform.stream.StreamSource))

(gen-class
  :name com.ivans.xslt.megatron.Transformer
  :init init
  :implements [com.ivans.xslt.megatron.transformer.XSLTTransformer]
  :constructors {[] []
                 [java.util.Map] []}
  :state state
  :prefix "tf-")

(defn xslt10?
  "Check if stylesheet uses XSLT 1.0 or something else"
  [ss]
  (let [source (sc/as-source ss)
        parsed (xml/parse source)]
    (= (-> parsed :attrs :version) "1.0")))

(defmulti new-transformer
  "Based on the configuration, return a new Saxon or Xalan transformer"
  (fn [cfg]
    (let [{stylesheets :stylesheets} cfg
          main-ss (first stylesheets)]
      (xslt10? main-ss))))
(defmethod new-transformer false [cfg]
  (com.ivans.xslt.megatron.SaxonTransformer. cfg))
(defmethod new-transformer true [cfg]
  (com.ivans.xslt.megatron.XalanTransformer. cfg))
; (gen-class
;   :name com.ivans.xslt.megatron.XalanTransformer
;   :implements [com.ivans.xslt.megatron.XSLTTransformer]
;   :init init
;   :constructors {}
;   :state state
;   :prefix "xalan-")

(defn tf-init
  [^java.util.Map cfg]
  (let [merged-config (mc/merge-config cfg)]
    [[] (atom {:xfmr (new-transformer merged-config)})]))

(defn tf-setConfiguration
  "Set a new configuration map. This reprocesses the config, so it will
  recompile the stylesheets and merge the new config into the existing one.
  Basically this 'resets' the transformer as if you were creating a new instance"
  [this ^java.util.Map cfg]
  (.setConfiguration (:xfmr @(.state this)) cfg))

(defn tf-updateConfiguration
  [this ^java.util.Map cfg]
  (.updateConfiguration (:xfmr @(.state this)) cfg))

(defn tf-getConfiguration
  [this]
  (.getConfiguration (:xfmr @(.state this))))

(defn tf-transform
  "Applies an XSLT transformation based on the configured options, either returns
  a String or it returns nil and writes to the specified file."
  ([this input]
   (.transform (:xfmr @(.state this)) input))
  ([this input output]
   (.transform (:xfmr @(.state this)) input output)))
