(ns com.ivans.xslt.megatron.transformer
  (:import (java.io File)
           java.util.Map
           (javax.xml.transform Source Transformer ErrorListener URIResolver)))
; (gen-interface
;   :name com.ivans.xslt.megatron.XSLTTransformer
;   :methods [[transform [java.io.File java.io.File] void]
;             [transform [javax.xml.transform.Source java.io.File] void]
;             [transform [java.io.File] String]
;             [transform [javax.xml.transform.Source] String]
;             [transform [String] String]
;             [updateConfiguration [java.util.Map] void]
;             [getConfiguration [] java.util.Map]
;             [setConfiguration [java.util.Map] void]])
(defprotocol XSLTTransformer
  (transform [input] [input output])
  (updateConfiguration [conf])
  (setConfiguration [conf]))
