(ns com.ivans.xslt.megatron.saxon
  (:require [clojure.string :as st]
            [com.ivans.xslt.megatron.core :as mc]
            [saxon.core :as sc]
            [saxon.xdm :as xdm]
            [saxon.xslt :as sn])
  (:import net.sf.saxon.s9api.RawDestination))

(def template-name (sc/->qname "http://www.w3.org/1999/XSL/Transform"
                               "initial-template"))
(defmulti flatten-result
  "When a transformation returns multiple XdmItems, converts them to strings and joins them with newlines"
  coll?)
(defmethod flatten-result true [result]
  (st/join "\n" (map str result)))
(defmethod flatten-result false [result]
  (str result))

(defn transform-fn
  "To handle possible multiple stylesheets.  Returns a function that applies a transformation
  and returns the result. This handles the fact that there's an extra Destination required
  when chaining transformations"
  ([transformer func]
   (fn [input]
     (let [result (apply func [transformer input])]
       (flatten-result (xdm/unwrap-xdm-items result)))))
  ([transformer func pipeline-dest]
   (fn [input final-dest]
     (apply func [transformer input pipeline-dest])
     (flatten-result (xdm/unwrap-xdm-items (.getXdmValue final-dest))))))

(defn prepare-multi-transformation
  "Rolls a collection of Xslt30Transformers into a Destination, returning the information
  needed to run a transformation and get the result.

  :main is the primary stylesheet that will be used to start the transformation
  :final-dest is the object that will eventually contain the transformation result
  :pipeline-dest is the rolled-up set of stylesheets that are passed into the applyTemplates or callTemplate
  method call as a Destination"
  [transformers]
  (let [[main-ss & other-ss] transformers
        dest (RawDestination.)
        xdest (sn/dest-reduce other-ss dest)]
    {:main main-ss
     :final-dest dest
     :pipeline-dest xdest}))

(defmulti set-initial-template-params (fn [main-ss input] (class input)))
(defmethod set-initial-template-params java.io.File
  [main-ss input]
  (let [params {:input-file (str (.toURI input))}]
    (sn/set-transformer-properties! main-ss {:init-template-params params})))
(defmethod set-initial-template-params java.net.URL
  [main-ss input]
  (let [params {:input-file (str (.toURI input))}]
    (sn/set-transformer-properties! main-ss {:init-template-params params})))
(defmethod set-initial-template-params java.lang.String
  [main-ss input]
  (let [params {:input-file input}]
    (sn/set-transformer-properties! main-ss {:init-template-params params})))
(defmethod set-initial-template-params java.net.URI
  [main-ss input]
  (let [params {:input-file (str input)}]
    (sn/set-transformer-properties! main-ss {:init-template-params params})))

(defn apply-templates
  "Use xform to apply templates to XML input."
  ([xform input]
   (->> input
        sc/as-source
        (.applyTemplates xform)))
  ([xform input dest]
   (let [src (sc/as-source input)]
     (.applyTemplates xform src dest))))

(defn call-template
  "Call xsl:initial-template with 'input' passed to a parameter"
  ([main-ss input]
   (set-initial-template-params main-ss input)
   (.callTemplate main-ss template-name))
  ([main-ss input dest]
   (set-initial-template-params main-ss input)
   (.callTemplate main-ss template-name dest)))

(defmulti transformation-function
  "based on the type of transformation and number of stylesheets, return a function that invokes the transformation
  in the appropriate way"
  (fn [transformers xml?]
    [(if xml? :xml :other)
     (if (> (count transformers) 1) :multi :single)]))

(defmethod transformation-function [:xml :single]
  [transformers _]
  (fn [input]
    (let [main-ss (first transformers)
          xfn (transform-fn main-ss apply-templates)]
      (xfn input))))

(defmethod transformation-function [:xml :multi]
  [transformers _]
  (let [{main-ss :main
         final :final-dest
         dest :pipeline-dest} (prepare-multi-transformation transformers)]
    (fn [input]
      (let [xfn (transform-fn main-ss apply-templates dest)]
        (xfn input final)))))

(defmethod transformation-function [:other :single]
  [transformers _]
  (let [main-ss (first transformers)]
    (fn [input]
      (let [xfn (transform-fn main-ss call-template)]
        (xfn input)))))

(defmethod transformation-function [:other :multi]
  [transformers _]
  (let [{main-ss :main
         final :final-dest
         dest :pipeline-dest} (prepare-multi-transformation transformers)]
    (fn [input]
      (let [xfn (transform-fn main-ss call-template dest)]
        (xfn input final)))))

(gen-class
  :name com.ivans.xslt.megatron.SaxonTransformer
  :implements [com.ivans.xslt.megatron.transformer.XSLTTransformer]
  :init init
  :constructors {[] []
                 [java.util.Map] []}
  :state state
  :prefix "saxon-")

(defn set-compiler-opts!
  "Compiler-level options are the ErrorListener, URIResolver, and any static parameters"
  [compiler options]
  (let [{err-listener :error-listener
         uri-resolver :uri-resolver
         utxt-uri-resolver :unparsed-text-uri-resolver
         params :static-params} options]
    (when err-listener (.setErrorListener compiler err-listener))
    (when uri-resolver (.setURIResolver compiler uri-resolver))
    (when utxt-uri-resolver (.. compiler
                                (getProcessor)
                                (getUnderlyingConfiguration)
                                (setUnparsedTextURIResolver utxt-uri-resolver)))
    (when (seq params) (sn/set-compiler-params! compiler params))))

(defn compile-stylesheets
  [compiler stylesheets]
  (map #(sn/transformer compiler %) stylesheets))

(defn set-stylesheet-params!
  "Set stylesheet-level parameters (not static params) on a stylesheet"
  [exe-ss params]
  (.setStylesheetParameters exe-ss (xdm/to-params params)))

(defn prepare-stylesheets
  "Return transformers that have been compiled and had the requested settings applied to them, along with
  a transformation function to be used in .transform"
  [compiler config]
  (let [{xml? :xml-input?
         stylesheets :stylesheets
         params :stylesheet-params
         msg-listener :message-listener} config]
    (when (seq stylesheets)
      (let [transformers (compile-stylesheets compiler stylesheets)
            trfm-fn (transformation-function transformers xml?)]
        (when (seq params) (run! #(set-stylesheet-params! % params) transformers))
        (when msg-listener (run! #(.setMessageListener % msg-listener) transformers))
        {:compiled transformers
         :fn trfm-fn}))))

(defn process-configuration
  [^java.util.Map cfg]
  (let [opts (mc/merge-config cfg)
        compiler (sn/compiler)]
    (set-compiler-opts! compiler opts)
    (let [ss-info (prepare-stylesheets compiler opts)]
      {:compiler compiler
       :transformers ss-info
       :configuration opts})))

(defn saxon-init
  [^java.util.Map cfg]
  [[] (atom (process-configuration cfg))])

(defn saxon-setConfiguration
  [this ^java.util.Map cfg]
  (let [state (.state this)]
    (reset! state (process-configuration cfg))))

(defn saxon-updateConfiguration
  [this ^java.util.Map cfg]
  (let [state (.state this)
        current (:configuration @this)
        new-cfg (merge current cfg)]
    (reset! state (process-configuration new-cfg))))

(defn saxon-getConfiguration
  [this]
  (:configuration @(.state this)))

(defn saxon-transform
  ([this input]
   (let [{{tr-fn :fn} :transformers} @(.state this)]
     (tr-fn input)))
  ([this input output]
   (let [result (saxon-transform this input)]
     (spit output result))))
