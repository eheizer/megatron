(ns com.ivans.xslt.megatron.xalan
  (:require [clojure.data.xml :as xml]
            [clojure.data.zip.xml :refer [text xml-> xml1->]]
            [clojure.java.io :as io]
            [clojure.string :as st]
            [com.ivans.xslt.megatron.core :as mc]
            [saxon.core :as sc]
            [saxon.sapling :as sp]
            [saxon.xdm :as xdm]
            [xalan.core :as xn])
  (:import java.io.File
           (javax.xml.transform
             ErrorListener
             Transformer
             TransformerException)
           net.sf.saxon.s9api.XdmDestination
           (net.sf.saxon.sapling
             Saplings
             SaplingText
             SaplingDocument)))

(gen-class
  :name com.ivans.xslt.megatron.XalanTransformer
  :implements [com.ivans.xslt.megatron.transformer.XSLTTransformer]
  :init init
  :constructors {[] []
                 [java.util.Map] []}
  :state state
  :prefix "xalan-")

(defn show-trace
  []
  (let [stack-trace (.getStackTrace (Thread/currentThread))]
    (map #(str (.getClassName %) "." (.getMethodName %)) stack-trace)))

(defn get-caller
  []
  (let [trace (show-trace)]
    (nth trace 6)))

(defn- forward-error-listener
  "Take instances of ErrorListener and MessageListener and return
  an ErrorListener that will forward xsl:messages to the MessageListener
  instance, or delegate to the ErrorListener instance"
  [err-listener msg-listener]
  (reify ErrorListener
    (^void error [_ ^TransformerException e]
      (.error err-listener e))
    (^void warning [_ ^TransformerException e]
      (if (st/ends-with? (get-caller) "message")
        (let [sl (.getLocator e)
              txt (.getMessage e)
              doc (sp/add-children (sp/doc) [(Saplings/text txt)])
              dbld (xdm/->xdm-val doc)]
          (.message msg-listener dbld false sl))
        (.warning err-listener e)))
    (^void fatalError [_ ^TransformerException e]
      (.fatalError err-listener e))))

(defn transform-fn
  "Choose the transformation function based on the number of input stylesheets. When
  there is more than one stylesheet, applies a reduce operation instead of transforming one at a time"
  [transformers]
  (cond
    (= (count transformers) 1) (fn [input]
                                 (xn/apply-ss (first transformers) input))
    (> (count transformers) 1) (xn/pipe transformers)))

(defn create-error-listener
  [err-listener msg-listener]
  (if msg-listener
    (forward-error-listener err-listener msg-listener)
    err-listener))

(defn set-compiler-opts!
  [compiler opts]
  (let [{uri-resolver :uri-resolver} opts]
    (when uri-resolver (.setURIResolver compiler uri-resolver))))

(defn prepare-stylesheets
  "Compile stylesheets and apply any requested settings to them"
  [opts]
  (let [{stylesheets :stylesheets
         msg-listener :message-listener
         provided-err-listener :error-listener
         params :stylesheet-params} opts
        err-listener (create-error-listener provided-err-listener msg-listener)
        transformers (map xn/compile-ss stylesheets)
        trfm-func (transform-fn transformers)]
    (when err-listener (run! #(.setErrorListener % err-listener) transformers))
    (when (seq params) (run! #(xn/set-params! % params) transformers))
    {:compiled transformers
     :fn trfm-func}))

(defn process-configuration
  [^java.util.Map cfg]
  (let [opts (mc/merge-config cfg)
        compiler xn/xfactory]
    (set-compiler-opts! compiler opts)
    (let [ssinfo (prepare-stylesheets opts)]
      {:transformers ssinfo
       :configuration opts})))

(defn xalan-init
  [^java.util.Map cfg]
  [[] (atom (process-configuration cfg))])

(defn xalan-setConfiguration
  [this ^java.util.Map cfg]
  (let [state (.state this)]
    (reset! state (process-configuration cfg))))

(defn xalan-updateConfiguration
  [this ^java.util.Map cfg]
  (let [state (.state this)
        current (:configuration @this)
        new-cfg (merge current cfg)]
    (reset! state (process-configuration new-cfg))))

(defn xalan-getConfiguration
  [this]
  (:configuration @(.state this)))

(defn xalan-transform
  ([this input]
   (let [xfn (-> @(.state this) :transformers :fn)]
     (xfn input)))
  ([this input output]
   (let [xfn (-> @(.state this) :transformers :fn)]
     (spit output (xfn input)))))
