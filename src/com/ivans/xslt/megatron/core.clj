(ns com.ivans.xslt.megatron.core)

(def ss-opts
  "Default configuration map for XSLTTransformers"
  {:xml-input? true
   :error-listener nil
   :message-listener nil
   :uri-resolver nil
   :unparsed-text-uri-resolver nil
   :stylesheets []
   :static-params {}
   :stylesheet-params {}})

(defn merge-config
  "Merge a provided config into the default, updating existing keys only"
  [hash]
  (let [valid-keys (filter #(contains? ss-opts (keyword %)) (keys hash))
        cfg (into {} (for [k valid-keys] [(keyword k) (get hash k)]))]
    (merge ss-opts cfg)))
