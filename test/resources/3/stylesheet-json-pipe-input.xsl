<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all" xmlns:map="http://www.w3.org/2005/xpath-functions/map" version="3.0">
    <!-- This is the default name for an initial template, but I don't care what it's called, we just need a convention -->
    <xsl:template name="xsl:initial-template">
        <xsl:param name="input-file" as="xs:string" select="''"/>
        <!-- path to file -->
        <xsl:variable name="parsed-json" select="json-doc($input-file)"/>
        <Header>
            <xsl:for-each select="map:keys($parsed-json?Header)" expand-text="yes">
                <xsl:element name="{.}">{$parsed-json?Header(.)}</xsl:element>
            </xsl:for-each>
        </Header>
    </xsl:template>
</xsl:stylesheet>
