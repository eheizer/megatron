<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all" xmlns:map="http://www.w3.org/2005/xpath-functions/map" version="3.0">
    <xsl:output method="text" omit-xml-declaration="yes"/>
    <!-- This is the default name for an initial template, but I don't care what it's called, we just need a convention -->
    <xsl:template name="xsl:initial-template" as="xs:string*">
        <xsl:param name="input-file" as="xs:string" select="''"/>
        <!-- path to file -->
        <xsl:variable name="parsed-json" select="json-doc($input-file)"/>
        <xsl:sequence select="map:keys($parsed-json)"/>
    </xsl:template>
</xsl:stylesheet>
