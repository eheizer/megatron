<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes"/>
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="Test">
        <xsl:message>Found "Test"</xsl:message>
        <xsl:copy>Hello from XSLT 1.0!</xsl:copy>
    </xsl:template>
</xsl:stylesheet>
