<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="Test">
        <xsl:variable name="first" select="substring-before(., 'is')"/>
        <xsl:variable name="last" select="substring-after(., 'is')"/>
        <xsl:copy>
            <xsl:value-of select="concat($first, 'was', $last)"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
