<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:param name="test"/>
    <xsl:template match="Test">
        <Root>
            <xsl:copy>
                <xsl:value-of select="concat('That ', substring-after(., 'This'))"/>
            </xsl:copy>
            <xsl:if test="$test">
                <Param>
                    <xsl:text>Found param: </xsl:text>
                    <xsl:value-of select="$test"/>
                </Param>
            </xsl:if>
        </Root>
    </xsl:template>
</xsl:stylesheet>
