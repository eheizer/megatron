(ns com.ivans.xslt.megatron-test
  (:require [clojure.java.io :as io]
            [clojure.string :as st]
            [clojure.test :refer :all]
            [com.ivans.xslt.megatron :refer :all]
            [saxon.core :as sc])
  (:import (com.ivans.xslt.megatron
             Transformer
             SaxonTransformer)
           com.ivans.xslt.megatron.transformer.XSLTTransformer
           java.net.URI
           (javax.xml.transform
             ErrorListener
             Source
             TransformerException
             SourceLocator)
           (net.sf.saxon.s9api
             XdmNode
             XdmDestination
             XsltCompiler
             MessageListener)))

(def sample-error-listener
  (binding [*out* *err*]
    (reify ErrorListener
      (^void error [_ ^TransformerException e]
        (println (str "Error: " (.getMessageAndLocation e))))
      (^void warning [_ ^TransformerException e]
        (println (str "Warning: " (.getMessageAndLocation e))))
      (^void fatalError [_ ^TransformerException e]
        (println (str "Fatal: " (.getMessageAndLocation e)))
        (throw e)))))

(def sample-message-listener
  (reify MessageListener
    (^void message [_
                    ^XdmNode content
                    ^boolean _terminate
                    ^SourceLocator locator]
      (let [rown (.getLineNumber locator)
            coln (.getColumnNumber locator)
            id (.getPublicId locator)
            _sys-id (.getSystemId locator)]
        (println (str "Message received at row: " rown ", col: "
                      coln " from " id ": " content))))))

(def sample-uri-resolver
  (reify javax.xml.transform.URIResolver
    (^Source resolve [_
                      ^String href
                      ^String base]
      (let [relative-uri (URI. href) ; create new URI from href
            base-uri (.toURI (io/file (if (empty? base) ; Get working directory if base dir isn't sent
                                        (System/getProperty "user.dir")
                                        base)
                                      "test/resources/3")) ; add the real resources directory to the file path
            resolved-uri (.resolve base-uri relative-uri)] ; resolve the two
        (sc/as-source resolved-uri))))) ; convert the URI to Source (uses .toURL().openStream() )

(def sample-uri-resolver1
  (reify javax.xml.transform.URIResolver
    (^Source resolve [_
                      ^String href
                      ^String base]
      (let [relative-uri (URI. href) ; create new URI from href
            base-uri (.toURI (io/file (if (empty? base) ; Get working directory if base dir isn't sent
                                        (System/getProperty "user.dir")
                                        base)
                                      "test/resources/1")) ; add the real resources directory to the file path
            resolved-uri (.resolve base-uri relative-uri)] ; resolve the two
        nil)))) ; convert the URI to Source (uses .toURL().openStream() )

(def xml-input (io/resource "xml-input.xml"))
(def conf-static-params {:xml-input? true
                         :error-listener sample-error-listener
                         :message-listener sample-message-listener
                         :uri-resolver nil
                         :stylesheets [(-> "3/stylesheet-xml-input.xsl" io/resource io/file)]
                         :static-params {:test true :string "A string"}
                         :stylesheet-params {}})

(def conf-xml-transform {:xml-input? true
                         :error-listener sample-error-listener
                         :message-listener sample-message-listener
                         :stylesheets [(-> "3/stylesheet-xml-input.xsl" io/resource io/file)]})

(def conf-xml1-transform {:xml-input? true
                          :error-listener sample-error-listener
                          :message-listener sample-message-listener
                          :stylesheets [(-> "1/stylesheet-xslt-1.0.xsl" io/resource io/file)]})

(def conf-text-transform {:xml-input? false
                          :stylesheets [(-> "3/stylesheet-json-input.xsl" io/resource io/file)]})

(def conf-text-pipeline {:xml-input? false
                         :stylesheets [(-> "3/stylesheet-json-pipe-input.xsl" io/resource io/file)
                                       (-> "3/stylesheet-json-pipe-input2.xsl" io/resource io/file)]})

(def conf-multiple {:xml-input? true
                    :error-listener sample-error-listener
                    :message-listener sample-message-listener
                    :uri-resolver nil
                    :stylesheets (->> ["3/A.xsl" "3/B.xsl" "3/C.xsl" "3/D.xsl" "3/E.xsl"] (map io/resource) (map io/file))})

(def conf-multiple1 {:xml-input? true
                     :stylesheets (->> ["1/A.xsl" "1/B.xsl" "1/C.xsl" "1/D.xsl"] (map io/resource) (map io/file))})

(deftest saxon-transformation
  (testing "standard XML transformation"
    (let [trf (Transformer. conf-xml-transform)
          result (.transform trf (io/file xml-input))]
      (is (st/includes? result "the text has now changed!"))))

  (testing "accepting static params"
    (let [trf (Transformer. conf-static-params)
          result (.transform trf (io/file xml-input))]
      (is (st/includes? result "String contents"))))

  (testing "transforming JSON input"
    (let [trf (Transformer. conf-text-transform)
          inp (str (io/resource "3/json-input.json"))
          result (.transform trf inp)]
      (is (= (st/join "\n" (st/split "5BPI03 5BPI04 5BPI37 5BPI38 Header 5BPI06 5BPI07 5BPI11 Name 5BPI01 5BPI12" #" ")) result))))

  (testing "multiple stylesheets"
    (let [trf (SaxonTransformer. conf-multiple)
          result (.transform trf (io/file xml-input))]

      (is (st/includes? result "That was a successful test! Hooray!"))))
  #_(testing "JSON to XML in pipeline"
      (let [trf (Transformer. conf-text-pipeline)
            result (.transform trf (io/resource "3/json-input.json"))]
        (is (= "foo" result)))))

(deftest xalan-transformations
  (testing "standard XML transformation"
    (let [trf (Transformer. conf-xml1-transform)
          input (io/file xml-input)
          result (.transform trf input)]
      (is (st/includes? result "Hello from XSLT 1.0!"))))
  (testing "input as string"
    (let [trf (Transformer. conf-xml1-transform)
          input (sc/as-source (io/file xml-input))
          result (.transform trf input)]
      (is (st/includes? result "Hello from XSLT 1.0!"))))
  (testing "multiple XML input"
    (let [trf (Transformer. conf-multiple1)
          input (io/file xml-input)
          result (.transform trf input)]
      (is (st/includes? result "That was a successful test! Hooray!!")))))
